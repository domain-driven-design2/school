package com.ics.school.infrastructure.dao;

import com.ics.school.domain.model.entity.Course;
import com.ics.school.domain.model.valueobject.CourseName;
import com.ics.school.domain.model.valueobject.Fee;
import com.ics.school.domain.model.valueobject.StudentId;
import com.ics.school.domain.repository.CourseRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CourseDao implements CourseRepository {

    List<Course> dbCourse = Arrays.asList(
            new Course(StudentId.of(1l), CourseName.of("English"), Fee.of(200d)),
            new Course(StudentId.of(2l), CourseName.of("French"), Fee.of(250d))
    );

    @Override
    public Course addCourse(Course course) {
        dbCourse.add(course);
        return getCourse(course.getId());
    }

    @Override
    public Course getCourse(StudentId id) {
        List<Course> students = dbCourse.stream().filter(student -> student.getId().toLong() == id.toLong()).collect(Collectors.toList());
        if(students.isEmpty()) {
            return null;
        }
        return students.get(0);
    }
}
