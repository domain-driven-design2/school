package com.ics.school.domain.repository;

import com.ics.school.domain.model.valueobject.StudentId;

public interface EnrollRepository {
    EnrollRepository addEnroll(EnrollRepository enroll);
    EnrollRepository getEnroll(StudentId id);
}
