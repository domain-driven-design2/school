package com.ics.school.domain.repository;

import com.ics.school.domain.model.entity.Course;
import com.ics.school.domain.model.valueobject.StudentId;

public interface CourseRepository {
    Course addCourse(Course course);
    Course getCourse(StudentId id);
}
