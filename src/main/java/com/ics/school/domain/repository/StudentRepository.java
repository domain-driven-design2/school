package com.ics.school.domain.repository;

import com.ics.school.domain.model.entity.Student;
import com.ics.school.domain.model.valueobject.StudentId;

public interface StudentRepository {
    Student getStudentById(StudentId id);
}
