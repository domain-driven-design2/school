package com.ics.school.domain.model.valueobject;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EnrollId {

    private long id;

    public static EnrollId of(long _id) {
        return new EnrollId(_id);
    }

    public long toLong() {
        return id;
    }

}
