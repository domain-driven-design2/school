package com.ics.school.domain.model.entity;

import com.ics.school.domain.model.valueobject.CourseId;
import com.ics.school.domain.model.valueobject.CourseName;
import com.ics.school.domain.model.valueobject.Fee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    private CourseId id;
    private CourseName courseName;
    private Fee fee;
}
