package com.ics.school.domain.model.entity;

import com.ics.school.domain.model.valueobject.EnrollId;
import com.ics.school.domain.model.valueobject.Fee;
import lombok.Data;

@Data
public class Enroll {

    private EnrollId enrollId;
    private Student student;
    private Course course;
    private Fee fee;
}
