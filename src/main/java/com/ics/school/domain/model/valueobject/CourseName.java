package com.ics.school.domain.model.valueobject;

import lombok.Value;

@Value
public class CourseName {
    private String courseName;

    private CourseName(String _courseName) {
        this.courseName = _courseName;
    }

    public static CourseName of(String _courseName) {
        return new CourseName(_courseName);
    }
}
