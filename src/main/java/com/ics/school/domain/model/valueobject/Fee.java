package com.ics.school.domain.model.valueobject;

import lombok.Value;

@Value
public class Fee {
    private double fee;

    private Fee(double fee) {
        this.fee = fee;
    }

    public static Fee of(double fee) {
        return new Fee(fee);
    }
}
