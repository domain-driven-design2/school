package com.ics.school.domain.model.valueobject;

import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;


@AllArgsConstructor(access = AccessLevel.PRIVATE)
final public class StudentId {

    private long id;

    public static StudentId of(long _id) {
        return new StudentId(_id);
    }

    @Getter
    public long toLong() {
        return id;
    }
}
