package com.ics.school.domain.model.entity;

import com.ics.school.domain.model.valueobject.StudentId;
import com.ics.school.domain.model.valueobject.Name;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private StudentId id;
    private StudentId idCard;
    private Name name;

}
