package com.ics.school.domain.model.valueobject;

import lombok.Value;

@Value
public class Name {

    private String name;

    private Name(String name) {
        this.name = name;
    }

    public static Name of(String _name) {
        return new Name(_name);
    }
}
