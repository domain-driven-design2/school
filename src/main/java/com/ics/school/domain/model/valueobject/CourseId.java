package com.ics.school.domain.model.valueobject;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
final public class CourseId {

    private long id;

    public static CourseId of(long _id) {
        return new CourseId(_id);
    }

    public long toLong() {
        return id;
    }

}
